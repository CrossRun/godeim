package main

import (
	"fmt"
	"goim/crossrun.top/server"
	"goim/crossrun.top/session"
	"log"
	"net/http"
	"time"
)

var sessionManager session.ISessionManager

func main() {
	sessionManager = session.NewCookieSessionManager()
	sessionManager.SetGCTime(60 * 60)
	//http.HandleFunc()
	serMux := server.NewServeMux()
	//serMux.HandleFunc("/",s)
	serMux.HandleFunc("/a", a)
	serMux.HandleFunc("/b", b)
	serMux.HandleFunc("/c", c)
	//serMux.FilterFunc("/b", fb)
	sf := server.NewServerFilter("filter")
	sf.FilterFunc("/a", fa)
	sf.FilterFunc("/b", server.EmptyFlyUpHandlerFilter)
	sf.FilterFunc("/c", server.EmptyBreakHandlerFilter)
	serMux.AddFilter(sf.GetId(), sf)
	log.Fatal(http.ListenAndServe(":9393", serMux))
	println(time.Now().Unix())
	fmt.Println(session.MEMORY)
	fmt.Println(session.REDIS)
	println(server.FLYUP)
	//smgr :=session.SessionManager{}
}

func s(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("sssss"))
}

func a(w http.ResponseWriter, r *http.Request) {
	sessionManager.BindSession(
		session.NewMemrorySession("ssss", 100),
		w,
	)
	session, _ := sessionManager.Get("ssss")
	session.Set("abc", "ddddddddddddddddddddddddddddddddddsdf")
	w.Write([]byte("aaaaa"))
}

func b(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("bbbbbb"))
}

func c(w http.ResponseWriter, r *http.Request) {
	if manager, ok := sessionManager.Get("ssss"); ok {
		w.Write([]byte("login\n"))
		p, _ := manager.Get("abc")
		w.Write([]byte(p.(string)))
	} else {
		w.Write([]byte("no login"))
	}
}

func fa(w http.ResponseWriter, r *http.Request) server.FILTER_RESULT {
	w.Write([]byte("done fa\n"))
	return server.CONTINUE
}

func fb(w http.ResponseWriter, r *http.Request) server.FILTER_RESULT {
	w.Write([]byte("done fb\n"))
	return server.BREAK
}

func fc(w http.ResponseWriter, r *http.Request) server.FILTER_RESULT {
	w.Write([]byte("done fc\n"))
	return server.FLYUP
}

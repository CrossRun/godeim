package session

import (
	"time"
)

type ISessionManager interface {
	Set(id string, manager ISession)
	Get(id string) (ISession, bool)
	Remove(id string)
	//销毁后不可在调用
	Destory()
	CreateId(param ...string) string
	//设置自动过期时间，默认不过期
	SetGCTime(gcTime int64)
	//BindSession(session ISession, w http.ResponseWriter, r *http.Request)
	BindSession(bundle ...interface{})
}

type ISession interface {
	Set(key, value interface{})
	Get(key interface{}) (interface{}, bool)
	Remove(key interface{})
	//销毁后不可在调用
	Destory()
	CreateId(param ...string) string
	//设置自动过期时间，默认不过期
	SetGCTime(maxAge int64)
	GetId() string
	LastAccessedTime() time.Time
	MaxAge() int64
}

type SessionType int

const (
	MEMORY SessionType = iota
	REDIS
)

type SessionManager struct {
	sessions map[string]ISessionManager
	sid      string
	maxAge   int64
}
